RASCIL contributors
===================

* **[Tim Cornwell](http://github.com/timcornwell)**

* **[Bojan Nikolic](http://github.com/bnikolic)**
  
* **[Peter Wortmann](http://github.com/scpmw)**

* **[Feng Wang](https://gitlab.com/cnwangfeng)**

* **[Vlad Stolyarov](https://gitlab.com/vlad7235)**

* **[Mark Ashdown](https://github.com/majashdown)**

* **[Danielle Fenech](https://github.com/daniellefenech)**
