.. _other_info:

******************
RASCIL development
******************

RASCIL is part of the SKA telescope organisation on GitLab https://gitlab.com/ska-telescope/rascil.git and development
is ongoing. We welcome merge requests submitted via GitLab. Guidelines and instructions for contributing to code and
documentation can be found here.


.. toctree::
   :maxdepth: 2

   RASCIL_developing
   RASCIL_documenting
   RASCIL_release_process
   RASCIL_background

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com
